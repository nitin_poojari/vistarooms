import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  enquiry: FormGroup;
  submitted = false;
  constructor(private fb: FormBuilder) { 
    this.enquiry = this.fb.group({
      fullname: ["", [Validators.required]],
      email: ["", [Validators.required, Validators.email]],
      mobile: ["", [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
      organization: ["", [Validators.required]],
      groupsize: ["", [Validators.required]],
      location: ["", [Validators.required]],
      timeslot: ["", [Validators.required]]
    });
  }

  ngOnInit(): void {
  }


  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

    // convenience getter for easy access to form fields
    get f() { return this.enquiry.controls; }

    on_submit_enquiry({ valid, value }) {
      this.submitted = true;
  
      // stop here if form is invalid
      if (this.enquiry.invalid) {
        return;
      } else {
        console.log(value)
      }
    }

}
